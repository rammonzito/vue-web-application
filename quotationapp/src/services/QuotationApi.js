import axios from 'axios';

const urlBaseQuotation = 'https://localhost:5001/api/quotation/';

export default {
    getAllQuotations: (limit, callback) => {
        axios.get(urlBaseQuotation, {
        }
        ).then((quotations) => {
            if (callback) {
                callback(quotations);
            }
        })
    }
}