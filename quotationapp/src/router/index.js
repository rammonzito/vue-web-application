import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
// import Quotation from '@/components/Quotation'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    }//,
    // {
    //   path: '/Quotation',
    //   name: 'Quotation',
    //   component: Quotation
    // }
  ]
})
